import "./Loader.css";
import LogoDead from "../img/logo_dead.svg";
import LoaderImg from "../img/marker.svg";

/**
 * Loader is a full-screen loading message.
 * @private
 * 
 * @param {CoreView} parent The parent view
 * @param {Element} container The DOM element to create loader into
 */
export default class Loader {
	constructor(parent, container) {
		this._parent = parent;
		this.container = container;
		this.container.classList.add("gvs-loader", "gvs-loader-visible");

		// Logo
		const logo = document.createElement("img");
		logo.src = LoaderImg;
		logo.title = this._parent._t.map.loading;
		logo.classList.add("gvs-loader-img");
		this.container.appendChild(logo);

		// Label
		const labelWrapper = document.createElement("div");
		const label = document.createElement("span");
		const nextLabel = () => (
			this._parent._t.gvs.loading_labels[
				Math.floor(Math.random() * this._parent._t.gvs.loading_labels.length)
			]
		);
		label.innerHTML = nextLabel();
		this._loaderLabelChanger = null;
		const nextLabelFct = () => setTimeout(() => {
			label.innerHTML = nextLabel();
			this._loaderLabelChanger = nextLabelFct();
		}, 500 + Math.random() * 2000);
		this._loaderLabelChanger = nextLabelFct();
		labelWrapper.appendChild(label);

		// Blinking dots
		for(let i=0; i < 3; i++) {
			const dot = document.createElement("span");
			dot.classList.add("gvs-loader-dot");
			dot.appendChild(document.createTextNode("."));
			labelWrapper.appendChild(dot);
		}

		this.container.appendChild(labelWrapper);
	}

	/**
	 * Dismiss loader, or show error
	 * @param {object} [err] Optional error object to show in browser console
	 * @param {str} [errMeaningful] Optional error message to show to user
	 */
	dismiss(err = null, errMeaningful = null) {
		clearTimeout(this._loaderLabelChanger);

		if(!err) {
			this.container.classList.remove("gvs-loader-visible");
			setTimeout(() => this.container.remove(), 2000);

			/**
			 * Event for viewer being ready to use (API loaded)
			 *
			 * @event ready
			 * @memberof CoreView
			 */
			const readyEvt = new CustomEvent("ready");
			this._parent.dispatchEvent(readyEvt);
		}
		else {
			if(err !== true) { console.error(err); }

			// Change content
			this.container.children[0].src = LogoDead;
			this.container.children[0].style.width = "200px";
			this.container.children[0].style.animation = "unset";
			let errHtml = errMeaningful;
			if(errHtml) { errHtml += "<br /><small>" + this._parent._t.gvs.error_subtitle + "</small>"; }
			else { errHtml = "<small>" + this._parent._t.gvs.error_subtitle + "</small>"; }
			this.container.children[1].innerHTML = `${this._parent._t.gvs.error}<br />${errHtml}`;
			const errLabel = errMeaningful || "GeoVisio had a blocking exception";

			/**
			 * Event for viewer failing to initially load
			 *
			 * @event broken
			 * @memberof CoreView
			 * @type {object}
			 * @property {object} detail Event information
			 * @property {string} detail.error The user-friendly error message to display
			 */
			const brokenEvt = new CustomEvent("broken", {
				detail: { error: errLabel }
			});
			this._parent.dispatchEvent(brokenEvt);

			// Throw error
			throw new Error(errLabel);
		}
	}
}