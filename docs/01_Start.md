# GeoVisio viewer hands-on guide

Welcome to GeoVisio __Viewer__ documentation ! It will help you through all phases of setup, run and develop on GeoVisio JS pictures viewer.

Also, if at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/geovisio/web-viewer/-/issues) or by [email](mailto:panieravide@riseup.net).


__Contents__

[[_TOC_]]


## Install the viewer

### NPM

GeoVisio viewer is available on NPM as [geovisio](https://www.npmjs.com/package/geovisio) package.

```bash
npm install geovisio
```

If you want the latest version (corresponding to the `develop` git branch), you can use the `develop` NPM dist-tag:

```bash
npm install geovisio@develop
```

### Hosted version

You can rely on various providers offering hosted NPM packages, for example JSDelivr.

```html
<!-- You may use another version than 2.5.1, just change the release in URL -->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/geovisio@2.5.1/build/index.css" />
<script src="https://cdn.jsdelivr.net/npm/geovisio@2.5.1/build/index.js"></script>
```

### Using this repository

You can install and use GeoVisio web client based on code provided in this repository.

This library relies on __Node.js 16__, which should be installed on your computer. Then, you can build the library using these commands:

```bash
npm install
npm build
```


## Basic usage

A GeoVisio viewer can be initialized with a basic HTML `div`:

```html
<div id="viewer" style="width: 500px; height: 300px"></div>
```

Then, the div is populated with this JavaScript code:

```js
var myViewer = new GeoVisio.Viewer(
	"viewer",  // Div ID
	"https://api.panoramax.xyz/api",  // STAC API endpoint
	{ map: true }  // Viewer options
);
```

Note that a map-only widget is also available (for showing service coverage for example):

```js
var myMap = new GeoVisio.StandaloneMap(
	"map",  // Div ID
	"https://api.panoramax.xyz/api"  // STAC API endpoint
);
```

All available options are listed in [this documentation](./02_Client_usage.md). You can also check out our [advanced practical examples](./04_Advanced_examples.md).


## User interface

### Keyboard shortcuts

All interactions with the viewer can be done using the mouse, and some can be also done using keyboard.

* __Arrows__ (or 2/4/8/6 on keypad): move inside the picture, or move the map
* __Page up/down__ (or 3/9 on keypad) : go to next or previous picture in sequence
* __+ / -__ : zoom picture or map in or out
* __*__ (or 5 on keypad) : move picture or map to its center
* __Home / ↖️__ (or 7 on keypad) : switch map and picture as main shown component
* __End__ (or 1 on keypad) : hide minimized map or picture
* __Space__ : play or pause current sequence


## Next steps

You may want to:

- Discover our [advanced practical examples](./04_Advanced_examples.md)
- Dive deep in [available JS methods](./02_Usage.md)
- Discover the available [URL settings](./03_URL_settings.md)
